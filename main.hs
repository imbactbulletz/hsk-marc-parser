import Text.Parsec
import System.Environment

data Document = Document Leader [Record] deriving (Show)

data Leader = Leader Code Value deriving (Show)

data Record = Record [Field] deriving (Show)

data Field = NonControlField Code [Indicator] [Subfield] | ControlField Code Value deriving (Show)

data Indicator = Indicator Char deriving (Show)

data Subfield = Subfield Code Value deriving (Show)

type Value = String
type Code = String


main :: IO()
main = do
  arguments <- getArgs
  input <- readFile (arguments !! 0)
  case (runParser document 0 (arguments !! 0) input ) of
    Left err -> putStrLn . show $ err
    Right document -> putStrLn (show document)


document :: Parsec String Int Document
document = do
 leader <- leader
 records <- many record
 return (Document leader records)


leader :: Parsec String Int Leader
leader = do
  code <- string "LDR"
  spaces
  value <- count 24 anyChar
  return (Leader code value)

record :: Parsec String Int Record
record = do
  fields <- many field
  char '\GS'
  return (Record fields)

field :: Parsec String Int Field
field = do
 code <- count 3 digit
 putState (read code :: Int)
 if((read code :: Integer ) < 10) then do
   spaces
   content <- many (noneOf ['\RS', '\GS'])
   char '\RS'
   return (ControlField code content)
 else do
   spaces
   indicators <- indicators
   spaces
   subfields <- many subfield
   char '\RS'
   return (NonControlField code indicators subfields)

indicators :: Parsec String Int [Indicator]
indicators = do
 indicator1 <- anyChar
 indicator2 <- anyChar
 return [(Indicator indicator1), (Indicator indicator2)]


subfield :: Parsec String Int Subfield
subfield = do
   char '$'
   code <- anyChar
   values <- many (noneOf ['$', '\RS'])
   return (Subfield [code] values)
